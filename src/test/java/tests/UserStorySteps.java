package tests;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import mumbai.common.restClient.RestClient;
import simulator.Actor;
import simulator.Token;

/**
 * Runs through the user stories required for the thing
 * @author s164166 Patrick
 */
public class UserStorySteps
{
	Actor customer;
	Actor merchant;
	Actor manager;
	Token token;
	RestClient clientPayout;
	RestClient clientToken;
	RestClient clientManagement;
	RestClient clientReport;

	JSONObject json;
	Response response;
	
	/**
	 * @author s164166 Patrick
	 */
	@Before
	public void initConnections()
	{
		clientPayout = new RestClient("http://02267-mumbai.compute.dtu.dk:4000/payout");
		clientToken = new RestClient("http://02267-mumbai.compute.dtu.dk:4001/tokens");
		clientManagement = new RestClient("http://02267-mumbai.compute.dtu.dk:4002/manager");
		clientReport = new RestClient("http://02267-mumbai.compute.dtu.dk:4003/report");

	}
	
	@After
	public void cleanup()
	{
		json = new JSONObject();

		{
			json.put("cpr", customer.getCPRNumber());
			clientPayout.post("delete", json.toString(), MediaType.APPLICATION_JSON);
		}
		json = new JSONObject();
		if (merchant != null)
		{
			json.put("cpr", merchant.getCPRNumber());
			clientPayout.post("delete", json.toString(), MediaType.APPLICATION_JSON);
		}
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Given("^a customer with the id \"([^\"]*)\"$")
	public void aCustomerWithTheId(String arg1) throws Throwable {
	    customer = new Actor("customer", "mc. customerson", arg1);
	    json = new JSONObject();
	    json.put("cpr", customer.getCPRNumber());
	    json.put("firstName", customer.getFirstName());
	    json.put("lastName", customer.getCPRNumber());
	    response = clientManagement.post("signup", json.toString(), MediaType.APPLICATION_JSON);
	    assertEquals(200, response.getStatus());
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Given("^a merchant with the id \"([^\"]*)\"$")
	public void aMerchantWithTheId(String arg1) throws Throwable {
	    merchant = new Actor("merchant", "mc. merchanterson", arg1);
	    json = new JSONObject();
	    json.put("cpr", merchant.getCPRNumber());
	    json.put("firstName", merchant.getFirstName());
	    json.put("lastName", merchant.getLastName());
	    response = clientManagement.post("signup", json.toString(), MediaType.APPLICATION_JSON);
	    assertEquals(200, response.getStatus());
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Given("^a manager with the id \"([^\"]*)\"$")
	public void aManagerWithTheId(String arg1) throws Throwable {
	    manager = new Actor("manager", "mc. managerson", arg1);
	    json = new JSONObject();
	    json.put("cpr", manager.getCPRNumber());
	    json.put("firstName", manager.getFirstName());
	    json.put("lastName", manager.getLastName());
	    response = clientManagement.post("signup", json.toString(), MediaType.APPLICATION_JSON);
	    assertEquals(200, response.getStatus());
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Given("^the customer has a bank account with (\\d+) money$")
	public void theCustomerHasABankAccountWithMoney(int arg1) throws Throwable {
		json = new JSONObject();
	    json.put("firstName", customer.getFirstName());
	    json.put("lastName", customer.getCPRNumber());
	    json.put("cpr", customer.getCPRNumber());
	    json.put("initialBalance", new BigDecimal(arg1));
	    response = clientPayout.post("create", json.toString(), MediaType.APPLICATION_JSON);
	    assertEquals(200, response.getStatus());
	}

	/**
	 * @author s164166 Patrick
	 */
	@Given("^the merchant has a bank account with (\\d+) money$")
	public void theMerchantHasABankAccountWithMoney(int arg1) throws Throwable {
		json = new JSONObject();
	    json.put("cpr", merchant.getCPRNumber());
	    json.put("firstName", merchant.getFirstName());
	    json.put("lastName", merchant.getLastName());
	    json.put("initialBalance", new BigDecimal(arg1));
	    response = clientPayout.post("create", json.toString(), MediaType.APPLICATION_JSON);
	    assertEquals(200, response.getStatus());
	}

	/**
	 * @author s164166 Patrick
	 */
	@Given("^the customer has (\\d+) token$")
	public void theCustomerHasToken(int arg1) throws Throwable {
	    json = new JSONObject();
	    json.put("userID", customer.getCPRNumber());
	    json.put("tokenAmount", arg1);
	    response = clientToken.post("dispatcher", json.toString(), MediaType.APPLICATION_JSON);
	   	    JSONObject result = new JSONObject(response.readEntity(String.class));
	    JSONArray tokenArray = (JSONArray) result.get("tokens");

		assertEquals(arg1, tokenArray.length());
		token = new Token(tokenArray.getString(0));
	}

	/**
	 * @author s164166 Patrick
	 */
	@When("^the merchant scans the token belonging to the customer for (\\d+) kroner$")
	public void theMerchantScansTheTokenBelongingToTheCustomerForKroner(int arg1) throws Throwable {
	    json = new JSONObject();
	    json.put("merchant", merchant.getCPRNumber());
	    json.put("amount", new BigDecimal(arg1));
	    json.put("token", token.getTokenID());
	    response = clientPayout.post("transaction", json.toString(), MediaType.APPLICATION_JSON);
	}

	/**
	 * @author s164166 Patrick
	 */
	@Then("^the payment suceeds$")
	public void thePaymentSuceeds() throws Throwable {
	    assertEquals(200, response.getStatus());
	}

	/**
	 * @author s164166 Patrick
	 */
	@Then("^the customer has (\\d+) money$")
	public void theCustomerHasMoney(int arg1) throws Throwable {
		json = new JSONObject();
	    json.put("cpr", customer.getCPRNumber());
	    response = clientPayout.post("getBalance", json.toString(), MediaType.APPLICATION_JSON);
	    assertEquals(200, response.getStatus());
	    json = new JSONObject(response.readEntity(String.class));
	    assertEquals(new BigDecimal(arg1), json.getBigDecimal("balance"));
	}

	/**
	 * @author s164166 Patrick
	 */
	@Then("^the merchant has (\\d+) money$")
	public void theMerchantHasMoney(int arg1) throws Throwable {
		json = new JSONObject();
	    json.put("cpr", merchant.getCPRNumber());
	    response = clientPayout.post("getBalance", json.toString(), MediaType.APPLICATION_JSON);
	    assertEquals(200, response.getStatus());
	    json = new JSONObject(response.readEntity(String.class));
	    assertEquals(new BigDecimal(arg1), json.getBigDecimal("balance"));
	}


	/**
	 * @author s164166 Patrick
	 */
	@Given("^a transaction where (\\d+) kroner was exchanged between the two$")
	public void aTransactionWhereKronerWasExchangedBetweenTheTwo(int arg1) throws Throwable {
	    BigDecimal value = new BigDecimal(arg1);
		json = new JSONObject();
	    json.put("merchant", merchant.getCPRNumber());
	    json.put("amount",value);
	    json.put("token", token.getTokenID());
	    response = clientPayout.post("transaction", json.toString(), MediaType.APPLICATION_JSON);
	    assertEquals(200, response.getStatus());

	}
	
	/**
	 * @author s164166 Patrick
	 */
	@When("^the merchant requests a refund using the token$")
	public void theMerchantRequestsARefundUsingTheToken() throws Throwable {
		json = new JSONObject();
	    json.put("merchant", merchant.getCPRNumber());
	    json.put("token", token.getTokenID());
	    response = clientPayout.post("refund", json.toString(), MediaType.APPLICATION_JSON);
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@Then("^the refund suceeds$")
	public void theRefundSuceeds() throws Throwable {
	    assertEquals(200, response.getStatus());
	}

	/**
	 * @author s164166 Patrick
	 */
	@When("^the manager requests a report$")
	public void theManagerRequestsAReport() throws Throwable {
		json = new JSONObject();
	    json.put("cpr", customer.getCPRNumber());
	    response = clientReport.post("report", json.toString(), MediaType.APPLICATION_JSON);
	}

	/**
	 * @author s164166 Patrick
	 */
	@Then("^the request suceeds$")
	public void theRequestSuceeds() throws Throwable {
	    assertEquals(200, response.getStatus());
	}
	
	/**
	 * @author s164166 Patrick
	 */
	@When("^the user uses the token to request a barcode$")
	public void theUserUsesTheTokenToRequestABarcode() throws Throwable {
		response = clientToken.get("barcode/"+token.getTokenID(), "image/png");
	}

	/**
	 * @author s164166 Patrick
	 */
	@Then("^receives an image with the barcode$")
	public void receivesAnImageWithTheBarcode() throws Throwable {
	    assertEquals(200, response.getStatus());
	}
}

