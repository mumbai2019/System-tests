#author s164166 Patrick
Feature: User Stories
  Scenario: Simple Payment
 		Given a customer with the id "MUMBAICUSTOMER"
		And a merchant with the id "MUMBAIMERCHANT"
		And the customer has a bank account with 100 money
		And the merchant has a bank account with 100 money
		And the customer has 1 token
		When the merchant scans the token belonging to the customer for 100 kroner
		Then the payment suceeds
		And the customer has 0 money
		And the merchant has 0 money
		
	Scenario: Refund transaction
		Given a customer with the id "MUMBAICUSTOMER2"
		And a merchant with the id "MUMBAIMERCHANT2"
		And the customer has a bank account with 100 money
		And the merchant has a bank account with 100 money
		And the customer has 1 token
		And a transaction where 100 kroner was exchanged between the two
		When the merchant requests a refund using the token
		Then the refund suceeds
		And the customer has 100 money
		And the merchant has 100 money
		
	Scenario: Report
		Given a customer with the id "MUMBAICUSTOMER3"
		And a merchant with the id "MUMBAIMERCHANT3"
		And a manager with the id "MUMBAIMANAGER3"
		And the customer has a bank account with 1000 money
		And the merchant has a bank account with 1000 money
		And the customer has 1 token
		And a transaction where 1000 kroner was exchanged between the two
		When the manager requests a report
		Then the request suceeds
		
	#author s172596 DIego
	Scenario: Existing user request barcode
    Given a customer with the id "MUMBAICUSTOMER4"
    And the customer has 1 token
    When the user uses the token to request a barcode
    Then the request suceeds