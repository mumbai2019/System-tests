package simulator;

/**
 * Used for generating a generalized token object for used in testing.
 * @author s164166 Patrick
 */
public class Token {
	
	private String tokenID;
	/**
	 * @author s164166 Patrick
	 */
	public Token(String string) {
		tokenID = string;
	}
	/**
	 * @author s164166 Patrick
	 */
	public String getTokenID() {
		return tokenID;
	}
	/**
	 * @author s164166 Patrick
	 */
	public void setTokenID(String tokenID) {
		this.tokenID = tokenID;
	}
}
