package simulator;

/**
 * Used to create a generalized model for a customer at an end point
 * @author s164166 Patrick
 */
public class Actor {
	
	private String firstName;
	private String lastName;
	private String CPRNumber;


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCPRNumber() {
		return CPRNumber;
	}

	public void setCPRNumber(String cPRNumber) {
		CPRNumber = cPRNumber;
	}
	
	public Actor(String firstname, String lastName, String CPRNumber)
	{
		this.firstName = firstname;
		this.lastName = lastName;
		this.CPRNumber = CPRNumber;
	}
}
