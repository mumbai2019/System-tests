System-tests
MVP Functionality:

Feature: Payment  
Scenario: Simple payment  
Given a registered customer with a bank account  
And a registered merchant with a bank account  
And the customer has one unused token  
When the merchant scans the token  
And requests payment for 100 kroner using the token  
Then the payment succeeds  
And the money is transferred from the  
customer bank account to the merchant bank account


- Merchant send valid token id
- Request payment for a given amount, using token:
- Payment is proccessed: 
    - Validate token
    - Check if merchant and customer exists
    - Transfer money from customer account to merchant account
    - Consume token
- Return "success" / "fail"    

Feature: Account Management  
Scenario: Costumer registration  
Given an unregistered costumer  
And a registered merchant  
When the costumer wants to make a payment  
Then the costumer will be registered  
And request tokens
And make the transaction  

- Costmer account is created
- Tokens are obtained
    - 
- Payment is proccessed: 
    - 
- Return "success" / "fail"     

Feature: Reporting  
Scenario: Costumer status report  
Given a registered costumer with a bank account  
And the user have made one or more payments  
When the costumer request a report giving a time period  
Then a list of transactions (amount of money, merchant, token) is returned  

- Costumer requets report
- Return list of transactions    

Feature: Reporting  
Scenario: Merchant status report  
Given a registered merchant with a bank account  
And the merchant have made one or more transactions  
When the merchan request a report giving a time period  
Then a list of transactions (amount of money, token) is returned  

- Merchant requets report
- Return list of transactions    


Prototype Jenkins build script for the External REST tests

set -e 
cd service 
mvn package
docker build --tag=rest . 
docker-compose up -d 
cd ../client
sleep 10s 
mvn test 
cd ../service 
docker-compose down 


Prototype DOCKERFILE


FROM fabric8/java-alpine-openjdk8-jre
COPY target/project-name-thorntail.jar /usr/src/
WORKDIR /usr/src/
CMD java -Djava.net.preferIPv4Stack=true\
-Djava.net.preferIPv4Addresses=true\
-jar project-name-thorntail.jar